package br.com.calculadora.calculadora.controllers;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.Services.CalculadoraService;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//classe que recebe a requisição pelo endpoint e efetua as validaçoes

@RestController
//O RestController é para o spring ja saber quando receber um proxy, é por aqui que tem que entrar.
//Ele faz isso automaticamente
//O @Controller é pra mvc, o RestController é pra Restful


@RequestMapping("/calculadora")
// <- O Request pega qualquer requisição. Utilizado geralemnte de forma generica no topo da classe.
//Nesse caso, qualquer requisição que venha, tipo localhost:8080/calculadora
//Independente se era um get ou um put/post



public class CalculadoraController {

    @Autowired
    //pega esse objeto unico que esta instanciado por baixo dos panos (CalculadoraService)  <- injeção de dependencia
    //O Spring que intancia automaticamente
    private CalculadoraService calculadoraService;

    //RequestBody quer dizer que é atraves dele (corpo) que vai chegar a requisicao
    //O controller que chama o service  <- na linha 18 que começa com o Autowired pra instanciar automaticamente.
    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário, pelo menos 2 numeros");
        } else {
            for (int n : calculadora.getNumeros()) {
                if (n < 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Envie números positivos");
                }
            }
            return calculadoraService.somar(calculadora);
        }
    }

    @PostMapping(value = "/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora ) {
        if (calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "É necessário, pelo menos 2 numeros");
        } else {
            for (int n : calculadora.getNumeros()) {
                if (n < 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Envie números positivos");
                }
            }
            return calculadoraService.subtrair(calculadora);
        }
    }


    @PostMapping("/multiplicar")
        public RespostaDTO multiplicar (@RequestBody Calculadora calculadora){
            if (calculadora.getNumeros().size() <= 1) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "É necessário, pelo menos 2 numeros");
            } else {
                for (int n : calculadora.getNumeros()) {
                    if (n < 0) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                "Envie números positivos");
                    }
                }
                return calculadoraService.multiplicar(calculadora);
            }
    }

    @PostMapping("/dividir")
        public RespostaDTO dividir(@RequestBody Calculadora calculadora){
            if (calculadora.getNumeros().size() <= 1) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "É necessário, pelo menos 2 numeros");
            } else {
                for (int n : calculadora.getNumeros()) {
                    if (n < 0) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie números positivos");
                    }
                }
                return calculadoraService.dividir(calculadora);
            }
        }
    }