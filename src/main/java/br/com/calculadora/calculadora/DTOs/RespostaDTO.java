package br.com.calculadora.calculadora.DTOs;

public class RespostaDTO {
    private int resultado;

    public RespostaDTO(Integer resultado) {
        this.resultado = resultado;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
}
